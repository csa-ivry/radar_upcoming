# radar_upcoming

A lightweight microservice for querying the https://radar.squat.net API.

## Features

- Show events for the next N days
- Cache results for some time (12 hours for now)
- Valid JSON output

## TODO

- Add some config file for cache parameters
- Add license

## Setup

radar_upcoming is a simple PHP script bundled with a router. Your webserver needs to pass URLs to index.php. For example, with nginx:

```location / {
	try_files $uri $uri/ /index.php?$args;
}```

## How to use it

https://ADDRESS.TLD/{city}/{group}[/{days}]
- city: return events in this city
- group: return events from this group
- (OPTIONAL) days: return events only for the next N days

For example, to show the next 5 days with events for the [Vaydom squat](https://radar.squat.net/en/node/143808) (group n° 143808), the query would be: `https://ADDRESS.TLD/Paris/143808/5`

## Integration with static-site generators

radar_upcoming should work just fine with your favorite static-site generator. We are pleasantly using it with [Hugo](https://gohugo.io). Here's an example template:

```{{ $days := getJSON "http://ADDRESS.TLD/Paris/143808/5" }}
{{ range $days }}
	<article>
		{{ $date := index . 0 "date_time" 0 "time_start" }}
		{{ $date := time $date }}
		<h3>{{ $date.Weekday }} {{ $date.Day }} {{ $date.Month }}</h3>
		{{ range . }}
			<a href="{{ index . "url" }}">{{ $date | dateFormat "15:04" }} {{ .title }}</a>
		{{ end }}
	</article>
{{ end }}```

### Translating dates

Dates cannot be natively translated by Hugo. However, it's not complicated to play around with some translation files. The current integration for Vaydom [uses](https://framagit.org/csa-ivry/vaytheme/blob/master/layouts/partials/sidebar.html) the following line : `<h3>{{ i18n (printf "day_%d" $date.Weekday) }} {{ $date.Day }} {{ i18n (printf "month_%d" $date.Month) }}</h3>`

The translation files that go along are available [here](https://framagit.org/csa-ivry/vaytheme/tree/master/i18n).

