<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Gilbitron\\Util\\' => array($vendorDir . '/gilbitron/php-simplecache/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Curl\\' => array($vendorDir . '/php-curl-class/php-curl-class/src/Curl'),
);
