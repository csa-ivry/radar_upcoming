<?php
// https://radar.squat.net/api/1.1/search/events.json?facets[city][]=Paris&facets[group][]=143808

require __DIR__ . '/vendor/autoload.php';

use \Curl\Curl;
use \Gilbitron\Util\SimpleCache;

function supportedLangs() {
    $langs = ['en', 'fr'];
    return implode('|', $langs);
}

function orderResults ($results, $days) {
	$ordered = [];
	
	foreach($results->result as $key => $value) {
    	unset($value->body->value);

	// Add link to the event
	$value->url = 'https://radar.squat.net/en/node/' . $key;
    	$start = $value->date_time[0]->time_start;
    	$day = substr($start,0,10);
    	
    	if (!array_key_exists($day, $ordered)) {
    		$ordered[$day] = [];
    	}
    	
    	$ordered[$day][] = $value;
    	
    } 
    
	uksort($ordered, function($a, $b) {
	  return strcmp($a, $b);
	});
	
	$numbered = [];
	
	foreach($ordered as $key => $value) {
		$numbered[] = $value;
	}
    
    # If we want event for the next N days, slice the array
    $upcoming = ($days ? array_slice($numbered, 0, $days) : $numbered);
    
    return json_encode($upcoming);
}

function fetchResults ($label, $address) {
	$cache = new Gilbitron\Util\SimpleCache();
	$cache->cache_path = 'cache/';
	$cache->cache_time = 43200;
	
	if($results = $cache->get_cache($label)){
		$results = json_decode($results);
	} else {
		$results = $cache->do_curl($address);
		$cache->set_cache($label, $results);
		$results = json_decode($results);
	}
	
	return $results;
}

function doRequest($args) {
	$days = (isset($args['days']) ? $args['days'] : false);
	$city = $args['city'];
	$group = $args['group'];
    $lang = $args['lang'];

	$label = $city.$group.$lang;
	$address = "https://radar.squat.net/api/1.1/search/events.json?facets[city][]={$city}&facets[group][]={$group}&language={$lang}";

	$results = fetchResults($label, $address);
	
	header('Content-Type: application/json');
	echo orderResults($results, $days);
}

function registerRoutes() {
	$dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
		$r->addRoute('GET', '/{lang:' . supportedLangs() . '}/{city}/{group:\d+}[/{days:\d+}]', 'doRequest');
	});
	return $dispatcher;
}

function dispatchRoutes($dispatcher) {
	// Fetch method and URI from somewhere
	$httpMethod = $_SERVER['REQUEST_METHOD'];
	$uri = $_SERVER['REQUEST_URI'];

	// Strip query string (?foo=bar) and decode URI
	if (false !== $pos = strpos($uri, '?')) {
		$uri = substr($uri, 0, $pos);
	}
	$uri = rawurldecode($uri);

	$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
	switch ($routeInfo[0]) {
		case \FastRoute\Dispatcher::NOT_FOUND:
			echo '404';
		    // ... 404 Not Found
		    break;
		case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
		    $allowedMethods = $routeInfo[1];
		    // ... 405 Method Not Allowed
		    break;
		case \FastRoute\Dispatcher::FOUND:
		    $handler = $routeInfo[1];
		    $vars = $routeInfo[2];
		    $handler($vars);
		    break;
	}
}

$dispatcher = registerRoutes();
dispatchRoutes($dispatcher);

?>
